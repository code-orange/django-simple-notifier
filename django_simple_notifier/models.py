from datetime import datetime

from django.db import models
from parler.models import TranslatableModel, TranslatedFields


class SnotifierEmailContact(models.Model):
    email = models.EmailField(max_length=100)
    date_added = models.DateTimeField(default=datetime.now)

    class Meta:
        abstract = True


class SnotifierEmailContactZammadAbstract(SnotifierEmailContact):
    ticket_id = models.PositiveIntegerField(null=True, blank=True)

    class Meta:
        abstract = True


class SnotifierEmailContactZammad(SnotifierEmailContactZammadAbstract):
    class Meta:
        abstract = False
        managed = False


class SnotifierSmsContact(models.Model):
    mobile = models.BigIntegerField()
    date_added = models.DateTimeField(default=datetime.now)

    class Meta:
        abstract = True


class SnotifierMsgType(models.Model):
    name = models.CharField(max_length=20, unique=True)

    class Meta:
        db_table = "snotifier_msg_type"


class SnotifierTemplates(models.Model):
    name = models.CharField(max_length=50, unique=True)
    template_en = models.TextField()

    class Meta:
        db_table = "snotifier_templates"


class SnotifierTemplate(TranslatableModel):
    name = models.CharField(max_length=50, unique=True)

    translations = TranslatedFields(template=models.TextField())

    class Meta:
        db_table = "snotifier_template"


class SnotifierTemplateTranslations(models.Model):
    template = models.ForeignKey(
        SnotifierTemplates, models.CASCADE, db_column="template"
    )
    lang_code = models.CharField(max_length=50)
    template_text = models.TextField()

    class Meta:
        db_table = "snotifier_template_translations"
        unique_together = (("template", "lang_code"),)
