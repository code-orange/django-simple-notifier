import slumber
from django.conf import settings


def send(
    notifier_list: list,
    subject,
    text,
    group="Sales",
    note=None,
    ticket_id=None,
    main_address=None,
    attachments=None,
    content_type="text/plain",
    closed=False,
):
    zammad_api = slumber.API(
        settings.ZAMMAD_URL, auth=(settings.ZAMMAD_USER, settings.ZAMMAD_PASSWD)
    )

    if ticket_id is None:
        ticketdata = dict()
        ticketdata["title"] = subject
        ticketdata["group"] = group

        if closed:
            ticketdata["state_id"] = 4

        if main_address is not None:
            ticketdata["customer_id"] = "guess:" + main_address
        else:
            ticketdata["customer_id"] = "guess:" + notifier_list[0].email

        ticketdata["article"] = dict()
        ticketdata["article"]["subject"] = subject

        if note is None:
            ticketdata["article"]["body"] = subject
        else:
            ticketdata["article"]["body"] = note

        ticketdata["article"]["type"] = "note"
        ticketdata["article"]["internal"] = False
        ticketdata["note"] = note

        ticket = zammad_api.tickets.post(ticketdata)
        ticket_id = int(ticket["id"])

    for recipient in notifier_list:
        recipient.ticket_id = ticket_id

        articledata = dict()
        articledata["ticket_id"] = ticket_id
        articledata["to"] = recipient.email
        articledata["subject"] = subject
        articledata["body"] = text
        articledata["content_type"] = content_type
        articledata["type"] = "email"
        articledata["internal"] = False

        if attachments is not None:
            articledata["attachments"] = attachments

        article = zammad_api.ticket_articles.post(articledata)

    return ticket_id
